REM Start program
REM 

for /f "usebackq tokens=1,2,3,*" %%A in (`netsh interface show interface`) do @if "%%D"=="Local Area Connection" set state=%%B

if "%state%"=="Connected" (
	netsh interface set interface "Local Area Connection" admin=disable
	netsh interface set interface "Wireless Network Connection" admin=enable
	reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Internet Settings" /v ProxyEnable /t REG_DWORD /d 0 /f
	TIMEOUT 5
) else (
	netsh interface set interface "Local Area Connection" admin=enable
	netsh interface set interface "Wireless Network Connection" admin=disable
	reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Internet Settings" /v ProxyEnable /t REG_DWORD /d 1 /f
	TIMEOUT 10
)

"C:\Program Files\Internet Explorer\iexplore.exe"

