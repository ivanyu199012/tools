REM SET the target directory, which you would like to assign the same right to 
REM all subfolder ( Not include the dir itself )
SET targetDir="C:\Users\admin\Desktop\Share"

REM SET the target user, to which you would like to assign the right
REM format targetUser="<AD login of the user"
SET targetUser="ivanTest"

REM SET the target user, to which you would like to assign the right
REM RX = read & execute ( Read only )
REM F = Full Control ( Read & Write )
SET permission=F

for /f "tokens=*" %%g in ('dir /s /b /o:n /ad %targetDir%') do Icacls "%%g" /grant %targetUser%:(OI)(CI)%permission%

PAUSE